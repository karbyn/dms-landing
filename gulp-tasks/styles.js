const fs             = require('fs'),
      gulp           = require('gulp'),
      notify         = require('gulp-notify'),
      plumber        = require('gulp-plumber'),
      sourcemaps     = require('gulp-sourcemaps'),
      sass           = require('gulp-sass'),
      sassGlob       = require('gulp-sass-glob');

const postcss        = require('gulp-postcss'),
      gradientFix    = require('postcss-gradient-transparency-fix'),
      cssdeclsort    = require('css-declaration-sorter'),
      autoprefixer   = require('autoprefixer'),
      cssnano        = require('cssnano'),
      cssbyebye      = require('css-byebye'),
      mqKeyframes    = require('postcss-mq-keyframes'),
      mqPacker       = require('css-mqpacker'),
      unmq           = require('postcss-unmq'),
      emMediaQuery   = require('postcss-em-media-query'),
      mediaQueryGap  = require('postcss-media-query-gap'),
      discardempty   = require('postcss-discard-empty');

const cfg             = JSON.parse(fs.readFileSync('./config.json')),
      srcStyles       = cfg.paths.src.styles,
      srcStylesPages  = cfg.paths.src.styles_pages,
      destStyles      = cfg.paths.dest.styles,
      destStylesPages = cfg.paths.dest.styles_pages,
      browserSupport  = cfg.browserSupport;





// Styles Default
gulp.task('styles', () => {
  gulp.src(srcStyles)
      .pipe(sassGlob())
      .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
      .pipe(sourcemaps.init())
      .pipe(sass())
      .pipe(postcss([
        mqPacker,
        cssbyebye({ rulesToRemove: ['.mqsort', '.foo'], map: false }),
        discardempty,
        mediaQueryGap,
        emMediaQuery,
        cssnano({ autoprefixer: false }),
        autoprefixer({ browsers: browserSupport })
      ]))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest(destStyles));
});
