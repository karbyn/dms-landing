const fs = require('fs'),
      path = require('path');
      gulp = require('gulp'),
      notify = require('gulp-notify'),
      plumber = require('gulp-plumber'),
      gulpWebpack = require('webpack-stream'),
      webpack = require('webpack'),
      uglify = require('gulp-uglify');

const cfg = JSON.parse(fs.readFileSync('./config.json')),
      srcScripts = cfg.paths.src.js,
      srcScriptsLib = cfg.paths.src.js_libs,
      destScripts = cfg.paths.dest.js;




// JS Dev Task
gulp.task('scripts', () => {
  gulp.src(srcScripts)
    .pipe(plumber({ errorHandler: notify.onError("Error: <%= error.message %>") }))
    .pipe(gulpWebpack({
      entry: {
        "app": srcScripts,
      },
      output: {
        filename: '[name].bundle.js',
      },
      devtool: 'source-map',
      watch: true,
      module: {
        loaders: [
          {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            loader: 'babel-loader',
            options: {
              presets: [
                ['es2015', { modules: false }]
              ]
            },
          },
        ],
      },
      plugins: [new webpack.optimize.UglifyJsPlugin()]
    }, webpack))
    .pipe(gulp.dest(destScripts));
});

