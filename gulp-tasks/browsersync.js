const fs = require('fs'),
      gulp = require('gulp'),
      browserSync = require('browser-sync');

const cfg = JSON.parse(fs.readFileSync('./config.json')),
      bsCfg = cfg.browserSync,
      watchCss = cfg.paths.watch.css;

gulp.task('browser-sync', () => {
  browserSync.init({
    server: {
        baseDir: "./public/web",
        index : "index.html"
    }
  });
});

gulp.task('css', () => {
  gulp.src(watchCss).pipe(browserSync.reload({ stream: true }));
});

gulp.task('bs-reload', () => {
  browserSync.reload();
});
