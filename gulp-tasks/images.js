const fs       = require('fs'),
      gulp     = require('gulp'),
      changed  = require('gulp-changed'),
      imagemin = require('gulp-imagemin');

const cfg      = JSON.parse(fs.readFileSync('./config.json')),
      srcImg   = cfg.paths.src.img,
      destImg  = cfg.paths.dest.img;



// minify new images
gulp.task('images',() => {
  gulp.src(srcImg)
      .pipe(changed(destImg))
      .pipe(imagemin({
            progressive: true,
            svgoPlugins: [
              {removeViewBox: false},
              {cleanupIDs: false}
            ]
          }))
      .pipe(gulp.dest(destImg));
});
