const fs = require('fs'),
      gulp = require('gulp'),
      watch = require('gulp-watch'),
      requireDir = require('require-dir'),
      browserSync = require('browser-sync');

const cfg = JSON.parse(fs.readFileSync('./config.json')),
      watchImg = cfg.paths.watch.img,
      watchFonts = cfg.paths.watch.fonts,
      watchScripts = cfg.paths.watch.js,
      watchStyles = cfg.paths.watch.styles,
      watchCss = cfg.paths.watch.css,
      watchTwig = cfg.paths.watch.twig,
      watchNunjucks = cfg.paths.watch.nunjucks,
      watchAssets = cfg.paths.watch.assets;

requireDir('./gulp-tasks');

gulp.task('default', [
  'images',
  'scripts',
  'styles',
  'watch',
  'browser-sync',
  'copy:fonts'
]);


// Watch gulp task
gulp.task('watch', () => {
  // watch for JS changes, then reload
  gulp.watch(watchScripts, ['scripts']).on('change', browserSync.reload);


  // watch for image changes
  gulp.watch(watchImg, ['images']);
  gulp.watch(watchFonts, ['copy:fonts']);

  // watch for SASS changes
  gulp.watch(watchStyles, ['styles']).on('change', browserSync.reload);

  // Watch for css changes, then inject css
  gulp.watch(watchCss, ['css']);
});
